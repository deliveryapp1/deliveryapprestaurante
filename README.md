# Sistema de Delivery - Restaurante.

## **Bibliotecas externas**

- _npm install br-mask --save -E_ [br-mask](https://github.com/amarkes/br-mask)

## Componentes customizados

# Validacão
- _validacao_ _<validacao [valor]="" mensagemError></validacao>_

# Monetário

- _ion-mask-money_ _<ion-mask-money></ion-mask-money>_

# Cep, Cpf, Cnpj, Telefone

 - _ion-mask_ _<ion-mask></ion-mask>_
