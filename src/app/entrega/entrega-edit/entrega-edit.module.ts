import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { EntregaEditPageRoutingModule } from './entrega-edit-routing.module';

import { EntregaEditPage } from './entrega-edit.page';

@NgModule({
  imports: [SharedModule, EntregaEditPageRoutingModule],
  declarations: [EntregaEditPage],
})
export class EntregaEditPageModule {}
