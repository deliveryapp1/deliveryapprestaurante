import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-entrega-edit',
  templateUrl: './entrega-edit.page.html',
  styleUrls: ['./entrega-edit.page.scss'],
})
export class EntregaEditPage implements OnInit {
  titulo = 'Nova Entrega';
  entrega = {
    id: null,
    bairro: '',
    tempoEntrega: '',
    entregaGratis: false,
    valorEntrega: 0.0,
  };
  constructor() {}

  ngOnInit() {}

  onSubmit() {}

  freteGratis() {
    this.entrega.entregaGratis = !this.entrega.entregaGratis;
    if (this.entrega.entregaGratis) {
      this.entrega.valorEntrega = null;
    }
  }
}
