import { AlertaService } from './../../core/services/alerta.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-entrega-list',
  templateUrl: './entrega-list.page.html',
  styleUrls: ['./entrega-list.page.scss'],
})
export class EntregaListPage implements OnInit {
  entregas: any[] = [];
  constructor(private alerta: AlertaService) {}

  ngOnInit() {
    for (let i = 1; i < 21; i++) {
      const entrega = {
        bairro: `Planalto`,
        tempoEntrega: `${10 * i}`,
        freteGratis: i % 2 === 0 ? true : false,
        valorEntrega: 10.0 * Math.floor(Math.random() * 5),
      };

      this.entregas.push(entrega);
    }
  }

  remover(entrega: any) {
    this.alerta.showConfirmDelete(entrega.bairro, () => this.executeRemove(entrega));
  }

  private executeRemove(entrega: any) {
    try {
      // chamar a api para remover
      // Removendo da tela
      const index = this.entregas.indexOf(entrega);
      this.entregas.splice(index, 1);

      this.alerta.showSucess('Entrega removida com sucesso');
    } catch (error) {
      this.alerta.showError('Erro ao remover a entrega');
    }
  }
}
