import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { EntregaListPageRoutingModule } from './entrega-list-routing.module';

import { EntregaListPage } from './entrega-list.page';

@NgModule({
  imports: [
    SharedModule,
    EntregaListPageRoutingModule
  ],
  declarations: [EntregaListPage]
})
export class EntregaListPageModule {}
