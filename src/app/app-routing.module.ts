import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardPageModule),
  },
  {
    path: 'cardapio',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./cardapio/cardapio-list/cardapio-list.module').then(
            (m) => m.CardapioListPageModule
          ),
      },
      {
        path: 'new',
        loadChildren: () =>
          import('./cardapio/cardapio-edit/cardapio-edit.module').then(
            (m) => m.CardapioEditPageModule
          ),
      },
      {
        path: 'edit/:id',
        loadChildren: () =>
          import('./cardapio/cardapio-edit/cardapio-edit.module').then(
            (m) => m.CardapioEditPageModule
          ),
      },
    ],
  },
  {
    path: 'categoria',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./categoria/categoria-list/categoria-list.module').then(
            (m) => m.CategoriaListPageModule
          ),
      },
      {
        path: 'new',
        loadChildren: () =>
          import('./categoria/categoria-edit/categoria-edit.module').then(
            (m) => m.CategoriaEditPageModule
          ),
      },
      {
        path: 'edit/:id',
        loadChildren: () =>
          import('./categoria/categoria-edit/categoria-edit.module').then(
            (m) => m.CategoriaEditPageModule
          ),
      },
    ],
  },
  {
    path: 'entrega',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./entrega/entrega-list/entrega-list.module').then((m) => m.EntregaListPageModule),
      },
      {
        path: 'new',
        loadChildren: () =>
          import('./entrega/entrega-edit/entrega-edit.module').then((m) => m.EntregaEditPageModule),
      },
      {
        path: 'edit/:id',
        loadChildren: () =>
          import('./entrega/entrega-edit/entrega-edit.module').then((m) => m.EntregaEditPageModule),
      },
    ],
  },
  {
    path: 'restaurante',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./restaurante/restaurante-list/restaurante-list.module').then(
            (m) => m.RestauranteListPageModule
          ),
      },
      {
        path: 'new',
        loadChildren: () =>
          import('./restaurante/restaurante-edit/restaurante-edit.module').then(
            (m) => m.RestauranteEditPageModule
          ),
      },
      {
        path: 'edit/:id',
        loadChildren: () =>
          import('./restaurante/restaurante-edit/restaurante-edit.module').then(
            (m) => m.RestauranteEditPageModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
