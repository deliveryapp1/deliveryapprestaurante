import { MinValueDirective } from './directives/min-value-.directive';
import { IonMessageValidatorComponent } from './components/ion-message-validator/ion-message-validator.component';
import { IonMaskComponent } from './components/ion-mask/ion-mask.component';
import { IonMaskMoneyComponent } from './components/ion-mask-money/ion-mask-money.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [
    IonMessageValidatorComponent,
    IonMaskComponent,
    IonMaskMoneyComponent,
    MinValueDirective,
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, IonicModule],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    IonMessageValidatorComponent,
    IonMaskComponent,
    IonMaskMoneyComponent,
    MinValueDirective,
  ],
})
export class SharedModule {}
