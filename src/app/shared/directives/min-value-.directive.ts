import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidationErrors } from '@angular/forms';

@Directive({
  selector: '[minValueValidation]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MinValueDirective, multi: true }],
})
export class MinValueDirective implements Validator {
  @Input('minValueValidation') minValue: number;
  constructor() {}
  validate(control: AbstractControl): ValidationErrors {
    return this.minMaxValueCurrency(control);
  }

  minMaxValueCurrency(control: AbstractControl) {
    if (control.value != null && this.minValue > control.value) {
      return { minValue: this.minValue };
    }

    return null;
  }
}
