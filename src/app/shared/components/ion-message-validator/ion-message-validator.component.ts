import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'ion-message-validator',
  templateUrl: './ion-message-validator.component.html',
  styleUrls: ['./ion-message-validator.component.scss'],
})
export class IonMessageValidatorComponent implements OnInit {
  @Input() mensagemError: string;
  @Input() control: FormControl;

  constructor() {}

  ngOnInit() {}
}
