import { Component, OnInit, forwardRef, Input } from '@angular/core';
import { CurrencyMask } from './currency-mask';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

/**
 * Mascara Monetária
 */
/**
 * @description
 * Comonente responsavel por criar mascaras especificas.
 * @usageNotes
 *
 * ### Implementação
 *
 * ```ts
 * <ion-mask-money
      name=""
      label=""
      typeLabel=""
      [(ngModel)]=""
    >
    </ion-mask-money>
 * ```
 */
@Component({
  selector: 'ion-mask-money',
  templateUrl: './ion-mask-money.component.html',
  styleUrls: ['./ion-mask-money.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => IonMaskMoneyComponent),
      multi: true,
    },
  ],
})
export class IonMaskMoneyComponent implements ControlValueAccessor {
  @Input() placeholder: string;
  @Input() label: string;
  @Input() typeLabel = '';
  @Input() clearInput = false;
  @Input() lastChild = false;
  @Input() disabled = false;

  public valueIonInput: string;

  private currencyMask = new CurrencyMask();
  private value: number;
  private propagateChange = (_: any) => {};

  constructor() {}

  public registerOnTouched() {}

  public writeValue(obj: number) {
    this.valueIonInput = this.currencyMask.detectAmountReverse(obj);
    if (obj) {
      this.keyUpEvent(null);
      this.value = obj;
    } else {
      this.value = null;
    }
  }

  public registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  public onChange(event) {
    if (this.valueIonInput) {
      this.value = Number.parseFloat(this.valueIonInput.replace(/[\.]/g, '').replace(/[,]/g, '.'));
    } else {
      this.value = null;
    }
    this.propagateChange(this.value);
  }

  public keyUpEvent(event) {
    this.valueIonInput = this.currencyMask.detectAmount(this.valueIonInput);
    this.onChange(event);
  }

  public getAttrLabel(type: string) {
    return this.typeLabel === type;
  }

  public getAttrClearInput() {
    return this.clearInput ? '' : null;
  }

  public getAttrPlaceholder() {
    return this.placeholder ? this.placeholder : '';
  }
}
