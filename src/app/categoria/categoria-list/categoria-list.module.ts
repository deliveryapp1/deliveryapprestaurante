import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CategoriaListPageRoutingModule } from './categoria-list-routing.module';

import { CategoriaListPage } from './categoria-list.page';

@NgModule({
  imports: [
SharedModule,
    CategoriaListPageRoutingModule
  ],
  declarations: [CategoriaListPage]
})
export class CategoriaListPageModule {}
