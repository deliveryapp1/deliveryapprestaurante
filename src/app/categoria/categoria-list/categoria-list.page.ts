import { AlertaService } from './../../core/services/alerta.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categoria-list',
  templateUrl: './categoria-list.page.html',
  styleUrls: ['./categoria-list.page.scss'],
})
export class CategoriaListPage implements OnInit {
  categorias: any[] = [];
  constructor(private alerta: AlertaService) {}

  ngOnInit() {
    for (let i = 0; i < 20; i++) {
      const categoria = {
        nome: `Categoria ${i + 1}`,
      };

      this.categorias.push(categoria);
    }
  }

  remover(categoria: any) {
    this.alerta.showConfirmDelete(categoria.nome, () => this.executeRemove(categoria));
  }

  private executeRemove(categoria: any) {
    try {
      // chamar a api para remover
      // Removendo da tela
      const index = this.categorias.indexOf(categoria);
      this.categorias.splice(index, 1);

      this.alerta.showSucess('categoria removida com sucesso');
    } catch (error) {
      this.alerta.showError('Erro ao remover a categoria');
    }
  }
}
