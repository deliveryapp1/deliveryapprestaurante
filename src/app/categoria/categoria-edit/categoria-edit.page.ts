import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categoria-edit',
  templateUrl: './categoria-edit.page.html',
  styleUrls: ['./categoria-edit.page.scss'],
})
export class CategoriaEditPage implements OnInit {
  titulo = 'Nova Categoria';

  categoria = {
    id: null,
    nome: '',
  };

  constructor() {}

  ngOnInit() {}

  onSubmit() {}
}
