import { SharedModule } from '../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CategoriaEditPageRoutingModule } from './categoria-edit-routing.module';

import { CategoriaEditPage } from './categoria-edit.page';

@NgModule({
  imports: [SharedModule, CategoriaEditPageRoutingModule],
  declarations: [CategoriaEditPage],
})
export class CategoriaEditPageModule {}
