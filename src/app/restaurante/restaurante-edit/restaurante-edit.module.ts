import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { RestauranteEditPageRoutingModule } from './restaurante-edit-routing.module';

import { RestauranteEditPage } from './restaurante-edit.page';

@NgModule({
  imports: [SharedModule, RestauranteEditPageRoutingModule],
  declarations: [RestauranteEditPage],
})
export class RestauranteEditPageModule {}
