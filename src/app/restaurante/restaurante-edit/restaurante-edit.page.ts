import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-restaurante-edit',
  templateUrl: './restaurante-edit.page.html',
  styleUrls: ['./restaurante-edit.page.scss'],
})
export class RestauranteEditPage implements OnInit {
  titulo = 'Novo Horario';

  restaurante = {
    id: null,
    semana: {},
    horaInicio: '',
    horaFim: '',
  };

  diasDaSemana = ([] = [
    { value: 'SEG', label: 'Segunda-feira' },
    { value: 'TER', label: 'Terça-feira' },
    { value: 'QUA', label: 'Quarta-feira' },
    { value: 'QUI', label: 'Quinta-feira' },
    { value: 'SEX', label: 'Sexta-feira' },
    { value: 'SAB', label: 'Sabádo' },
    { value: 'DOM', label: 'Domingo' },
  ]);

  constructor() {}

  ngOnInit() {}

  onSubmit() {}
}
