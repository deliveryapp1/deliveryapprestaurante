import { AlertaService } from './../../core/services/alerta.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-restaurante-list',
  templateUrl: './restaurante-list.page.html',
  styleUrls: ['./restaurante-list.page.scss'],
})
export class RestauranteListPage implements OnInit {
  restaurantes: any[] = [];

  constructor(private alerta: AlertaService) {}

  ngOnInit() {
    for (let i = 0; i < 20; i++) {
      const restaurante = {
        diaSemana: `Segunda`,
        horaInicio: '07:43',
        horaFim: '07:43',
      };

      this.restaurantes.push(restaurante);
    }
  }

  remover(entrega: any) {
    this.alerta.showConfirmDelete(entrega.diaSemana, () => this.executeRemove(entrega));
  }

  private executeRemove(entrega: any) {
    try {
      // chamar a api para remover
      // Removendo da tela
      const index = this.restaurantes.indexOf(entrega);
      this.restaurantes.splice(index, 1);

      this.alerta.showSucess('Entrega removida com sucesso');
    } catch (error) {
      this.alerta.showError('Erro ao remover a entrega');
    }
  }
}
