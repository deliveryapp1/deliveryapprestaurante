import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { RestauranteListPageRoutingModule } from './restaurante-list-routing.module';

import { RestauranteListPage } from './restaurante-list.page';

@NgModule({
  imports: [SharedModule, RestauranteListPageRoutingModule],
  declarations: [RestauranteListPage],
})
export class RestauranteListPageModule {}
