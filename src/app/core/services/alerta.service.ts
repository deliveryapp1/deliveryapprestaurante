import { Injectable } from '@angular/core';
import { ToastController, AlertController, LoadingController } from '@ionic/angular';
import { AlertOptions, LoadingOptions, ToastOptions } from '@ionic/core';

@Injectable({
  providedIn: 'root',
})
export class AlertaService {
  constructor(
    private toastController: ToastController,
    private alertController: AlertController,
    private loadingController: LoadingController
  ) {}

  private async alert(options?: AlertOptions): Promise<HTMLIonAlertElement> {
    const alert = await this.alertController.create(options);
    await alert.present();
    return alert;
  }

  private async loading(options?: LoadingOptions): Promise<HTMLIonLoadingElement> {
    const loading = await this.loadingController.create({
      message: 'Aguarde...',
      ...options,
    });

    await loading.present();
    return loading;
  }

  private async toast(options?: ToastOptions): Promise<HTMLIonToastElement> {
    const toast = await this.toastController.create({
      position: 'bottom',
      duration: 3000,
      keyboardClose: true,
      ...options,
    });

    await toast.present();
    return toast;
  }

  async showSucess(texto: string) {
    return this.toast({
      message: texto,
      color: 'sucess',
      keyboardClose: true,
    });
  }

  async showError(texto: string) {
    return this.toast({
      message: texto,
      color: 'danger',
      keyboardClose: true,
    });
  }

  async showConfirmDelete(nome: string, acao: () => void) {
    this.alert({
      header: 'Atenção...',
      message: `Deseja remover o ${nome}?`,
      buttons: [
        'Cancelar',
        {
          text: 'Remover',
          handler: () => {
            acao();
          },
        },
      ],
    });
  }
}
