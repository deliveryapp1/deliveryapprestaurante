import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-cardapio-edit',
  templateUrl: './cardapio-edit.page.html',
  styleUrls: ['./cardapio-edit.page.scss'],
})
export class CardapioEditPage implements OnInit {
  titulo = 'Novo Produto';

  produto = {
    id: null,
    foto: '',
    nome: '',
    preco: '0,00',
    descricao: '',
    telefone: '',
    categoria: {},
  };

  categoriaList = [
    {
      id: 1,
      nome: 'Bebida',
    },
    {
      id: 2,
      nome: 'Lanche',
    },
  ];

  constructor() {}

  ngOnInit() {}

  carregarImagem() {}

  onSubmit() {}
}
