import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CardapioEditPageRoutingModule } from './cardapio-edit-routing.module';
import { CardapioEditPage } from './cardapio-edit.page';

@NgModule({
  imports: [SharedModule, CardapioEditPageRoutingModule],
  declarations: [CardapioEditPage],
})
export class CardapioEditPageModule {}
