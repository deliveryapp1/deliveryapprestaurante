import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CardapioListPageRoutingModule } from './cardapio-list-routing.module';

import { CardapioListPage } from './cardapio-list.page';

@NgModule({
  imports: [
    SharedModule,
    CardapioListPageRoutingModule
  ],
  declarations: [CardapioListPage]
})
export class CardapioListPageModule {}
