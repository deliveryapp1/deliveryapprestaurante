import { AlertaService } from './../../core/services/alerta.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cardapio-list',
  templateUrl: './cardapio-list.page.html',
  styleUrls: ['./cardapio-list.page.scss'],
})
export class CardapioListPage implements OnInit {
  produtos: any[] = [];

  constructor(private alerta: AlertaService) {}

  ngOnInit() {
    for (let i = 0; i < 20; i++) {
      const produto = {
        categoria: 'Hambúrguers',
        nome: `Hambúrguer ${i + 1}`,
        preco: 10.5 * (i + 1),
        descricao: `Pão artesanal de brioche, suculento blend de costela,
          cheddar cremoso, barbecue da casa, maionese da casa,
          crispy de bacon, onion rings`,
        photoUrl: 'https://source.unsplash.com/sc5sTPMrVfk/640x531',
      };

      this.produtos.push(produto);
    }
  }

  remover(produto: any) {
    this.alerta.showConfirmDelete(produto.nome, () => this.executeRemove(produto));
  }

  private executeRemove(produto: any) {
    try {
      // chamar a api para remover

      // Removendo da tela
      const index = this.produtos.indexOf(produto);
      this.produtos.splice(index, 1);

      this.alerta.showSucess('Produto removido com sucesso');
    } catch (error) {
      this.alerta.showError('Erro ao remover o produto');
    }
  }
}
